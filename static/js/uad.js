function get_rss_info(url, div) {
    $.ajax({
        type: "GET",
        url: document.location.protocol + '//api.rss2json.com/v1/api.json?rss_url=' + encodeURIComponent(url),
        dataType: 'json',
        error: function(){
            //alert('Unable to load feed, Incorrect path or invalid feed');
        },
        success: function(response){
            var regex = /<img[^>]+src=[\"|']([^\"]*)[\"|']/;
            var cont = 1;
            response.items.slice(0,3).forEach(function(row){
                var images_all = regex.exec(row.content);

                if( $.isArray(images_all)) {
                    var image = images_all[0]+">";
                }
                else{
                    var image = "<img src='"+ row.thumbnail +"'>";
                }
                $("#"+div+cont+" p.title-as-summary").text(row.title);
                $("#"+div+cont+" .overlay-container > a").attr("href", row.link);
                $("#"+div+cont+" .overlay-container").css("background", 'url('+$(image).attr("src")+') center center no-repeat');
                $("#"+div+cont+" a.link-dark").attr("href", row.link);
                cont = cont + 1;
            });
        }
	});
}